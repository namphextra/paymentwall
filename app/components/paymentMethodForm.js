import React from 'react';
import { Form, Input, Button, notification } from 'antd';
import PropTypes from 'prop-types';
import APIFactory from '@api/factory';
import Utils from '../utils/currency';

const CurrencyAPI = APIFactory.get('currency');

class PaymentMethodForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchedMoney: false,
      money: 100,
      form: {
        cardHolderName: '',
        cardNumber: 0,
        expirationDate: '',
        cvv: 0,
      },
    };
    this.validateCharacterOnly = this.validateCharacterOnly.bind(this);
    this.validateCardNumber = this.validateCardNumber.bind(this);
    this.validateExpiration = this.validateExpiration.bind(this);
    this.validateCVV = this.validateCVV.bind(this);
    this.onFinishForm = this.onFinishForm.bind(this);
    this.onFinishFailedForm = this.onFinishFailedForm.bind(this);
  }

  onFinishForm() {
    notification.success({
      message: 'Paid',
      description: 'Your payment is successful',
      duration: 3,
    });
  }

  onFinishFailedForm() {
    notification.error({
      message: 'Failed',
      description: 'Your payment method is invalid',
      duration: 3,
    });
  }

  validateCharacterOnly = (rule, value) => {
    if (value && value.match(/[^a-zA-Z\s]/g)) {
      return Promise.reject(
        new Error('Card holder just include characters only'),
      );
    }

    return Promise.resolve();
  };

  validateCardNumber = (rule, value) => {
    if (!value) {
      return Promise.reject(new Error('Please fill your card number'));
    }

    if (value.match(/[^\d]/g)) {
      return Promise.reject(new Error('Please input only number'));
    }

    // Luhn algorithm
    const arrayConverted = [...value].map(v => Number(v));
    let sum = 0;
    for (let i = 0; i < arrayConverted.length; i += 1) {
      const currentValue = arrayConverted[arrayConverted.length - 1 - i];
      if ((arrayConverted.length - 1 - i) % 2 === 0 && currentValue * 2 >= 10) {
        sum += [...String(currentValue * 2)].reduce(
          (acc, val) => acc + Number(val),
          0,
        );
      } else {
        sum += currentValue;
      }
    }

    if (sum % 10 !== 0) {
      return Promise.reject(new Error('Your number card is invalid'));
    }

    return Promise.resolve();
  };

  validateExpiration = (rule, value) => {
    console.log(22222);
    if (!value) {
      return Promise.reject(new Error('Please fill your card number'));
    }

    const valueArray = [...value.replace(/[^\d]/g, '')];
    let expirationMonth = '';
    const expirationYear = '';

    if (
      Number(valueArray[0]) > 1 ||
      (Number(valueArray[0]) === 1 && Number(valueArray[1]) > 2)
    ) {
      expirationMonth = `0${valueArray[0]}`;
    } else {
      expirationMonth = `${valueArray[0]}${valueArray[1]}`;
    }

    this.setState(state => ({
      form: {
        ...state.form,
        expirationDate:
          expirationYear.length > 0
            ? `${expirationMonth}/${expirationYear}`
            : expirationMonth,
      },
    }));

    const localeDateArray = new Date().toLocaleDateString().split('/');
    if (
      Number(this.state.form.expirationDate.year) <
      Number(localeDateArray[2] % 100)
    ) {
      return Promise.reject(
        new Error("Your card's expiration year is in the past"),
      );
    }

    if (
      this.state.form.expirationDate.year ===
        Number(localeDateArray[2]) % 100 &&
      Number(this.state.form.expirationDate.month) < Number(localeDateArray[0])
    ) {
      return Promise.reject(
        new Error("Your card's expiration month is in the past"),
      );
    }
    return Promise.resolve();
  };

  validateCVV = (rule, value) => {
    if (!value) {
      return Promise.reject(new Error('Please fill your CVC'));
    }

    if (value.match(/[^\d]/g)) {
      return Promise.reject(new Error('Please input just number'));
    }

    if (value.length < 3) {
      return Promise.reject(new Error('CVV need three numbers'));
    }

    return Promise.resolve();
  };

  async initMoney() {
    if (this.props.country.alpha2Code !== 'USD') {
      let currencyRate = sessionStorage.getItem('currencyRate');
      let rate = 0;

      if (currencyRate) {
        currencyRate = JSON.parse(currencyRate);
        if (currencyRate[this.props.country.alpha2Code]) {
          rate = currencyRate[this.props.country.alpha2Code];
        } else {
          const result = await CurrencyAPI.getRate(
            'USD',
            this.props.country.currencies[0].code,
          );
          rate = result.data[`USD_${this.props.country.currencies[0].code}`];
          currencyRate[this.props.country.alpha2Code] = rate;
        }
      } else {
        currencyRate = {};
        const result = await CurrencyAPI.getRate(
          'USD',
          this.props.country.currencies[0].code,
        );
        rate = result.data[`USD_${this.props.country.currencies[0].code}`];
        currencyRate[this.props.country.alpha2Code] = rate;
      }
      sessionStorage.setItem('currencyRate', JSON.stringify(currencyRate));
      this.setState({
        money: rate * 100,
      });
    } else {
      this.setState({
        money: 100,
      });
    }
    this.setState({
      fetchedMoney: true,
    });
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps) !== JSON.stringify(this.props))
      this.initMoney();
  }

  componentDidMount() {
    this.initMoney();
  }

  render() {
    return (
      <>
        <Form
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={this.onFinishForm}
          onFinishFailed={this.onFinishFailedForm}
        >
          <Form.Item
            label="Card holder name"
            name="cardHolderName"
            rules={[
              { validator: this.validateCharacterOnly },
              { required: true, message: 'Please fill your card holder name' },
            ]}
          >
            <Input placeholder="Your card holder name" />
          </Form.Item>
          <Form.Item
            label="Card number"
            name="cardNumber"
            rules={[{ validator: this.validateCardNumber }]}
          >
            <Input maxLength={16} placeholder="Your card number" />
          </Form.Item>
          <Form.Item
            label="Expiration date"
            name="expirationMonth"
            rules={[
              {
                validator: (rule, value) =>
                  this.validateExpiration(rule, value),
              },
            ]}
            value={this.state.form.expirationDate}
          >
            <Input maxLength={5} placeholder="MM/YY" />
          </Form.Item>
          <Form.Item
            label="CVV"
            name="cvv"
            rules={[{ validator: this.validateCVV }]}
          >
            <Input maxLength={3} placeholder="CVV" />
          </Form.Item>
          {this.state.fetchedMoney ? (
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Pay{' '}
                {Utils.formatCurrency(
                  this.state.money,
                  `${this.props.country.languages[0].iso639_1}-${
                    this.props.country.alpha2Code
                  }`,
                  this.props.country.currencies[0].code,
                )}
              </Button>
            </Form.Item>
          ) : null}
        </Form>
      </>
    );
  }
}

PaymentMethodForm.propTypes = {
  country: PropTypes.object,
};

export default PaymentMethodForm;
