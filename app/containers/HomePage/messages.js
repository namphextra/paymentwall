/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.HomePage';

export default defineMessages({
  country: {
    id: `${scope}.country`,
    defaultMessage: 'Choose your country',
  },
  paymentMethods: {
    label: {
      id: `${scope}.paymentMethods.label`,
      defaultMessage: 'Choose your payment method',
    },
  },
});
