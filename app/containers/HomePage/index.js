import React from 'react';
import PublicIP from 'public-ip';
import IPLocate from 'node-iplocate';
import { Select } from 'antd';
import { FormattedMessage } from 'react-intl';
import APIFactory from '@api/factory';
import messages from './messages';
import PaymentMethodForm from '../../components/paymentMethodForm';

const APICountry = APIFactory.get('country');
const APIPayment = APIFactory.get('payment');
const { Option } = Select;

export default class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetched: false,
      currentCountryCode: '',
      countryList: [],
      paymentMethods: [],
    };

    this.init();
    this.onChangeSelect = this.onChangeSelect.bind(this);
  }

  async init() {
    await Promise.all([this.initCountryList(), this.initCurrentCountry()]);

    this.setState({
      fetched: true,
    });
  }

  async initCountryList() {
    const res = await APICountry.getList();
    this.setState({
      countryList: res.data,
    });
  }

  async initCurrentCountry() {
    const result = await PublicIP.v4();
    const countryCodeResult = await IPLocate(result);
    const paymentMethods = await APIPayment.getPaymentMethods(
      countryCodeResult.country_code,
    );
    this.setState({
      currentCountryCode: countryCodeResult.country_code,
      paymentMethods: paymentMethods.data,
    });
  }

  async onChangeSelect(e) {
    this.setState({
      currentCountryCode: e,
    });
    const paymentMethods = await APIPayment.getPaymentMethods(e);
    this.setState({
      paymentMethods: paymentMethods.data,
    });
  }

  render() {
    return (
      <div className="container mx-auto">
        {this.state.fetched ? (
          <>
            <h2>
              <FormattedMessage {...messages.country} />
            </h2>
            <Select
              defaultValue={this.state.currentCountryCode}
              className="w-full"
              onChange={this.onChangeSelect}
            >
              {this.state.countryList.map(country => (
                <Option value={country.alpha2Code} key={country.alpha2Code}>
                  {country.name}
                </Option>
              ))}
            </Select>
            <h2>
              <FormattedMessage {...messages.paymentMethods.label} />
            </h2>
            <div className="payment-methods__container flex">
              {this.state.paymentMethods.map(paymentMethod => (
                <div className="payment-method" key={paymentMethod.id}>
                  <img src={paymentMethod.img_url} alt={paymentMethod.id} />
                </div>
              ))}
            </div>
            <PaymentMethodForm
              country={this.state.countryList.find(
                v => v.alpha2Code === this.state.currentCountryCode,
              )}
            />
          </>
        ) : (
          <div />
        )}
      </div>
    );
  }
}
