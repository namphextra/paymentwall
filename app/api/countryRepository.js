import axios from 'axios';

const baseURL =
  'https://restcountries.eu/rest/v2/all?fields=alpha2Code;name;currencies;languages';

const axiosCreator = axios.create({
  baseURL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default {
  getList() {
    return axiosCreator.get('');
  },
};
