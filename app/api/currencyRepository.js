import axios from 'axios';
import Config from '../config';

const baseURL = 'https://free.currconv.com/api/v7';

const axiosCreator = axios.create({
  baseURL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default {
  getRate(currentCurrency, targetCurrency) {
    return axiosCreator.get(
      `/convert?compact=ultra&q=${currentCurrency}_${targetCurrency}&apiKey=${Config.config.get(
        'currencyAPIKey',
      )}`,
    );
  },
};
