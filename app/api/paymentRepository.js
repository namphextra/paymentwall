import axios from 'axios';
import Config from '../config/index';

const baseURL = 'https://api.paymentwall.com/api';
const axiosCreator = axios.create({
  baseURL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default {
  getPaymentMethods(countryCode) {
    return axiosCreator.get(
      `/payment-systems?key=${Config.config.get(
        'paymentWallProjectKey',
      )}&country_code=${countryCode}&sign_version=${Config.config.get(
        'paymentWallSignVersion',
      )}`,
    );
  },
};
