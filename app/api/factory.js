import CountryRepository from './countryRepository';
import PaymentRepository from './paymentRepository';
import CurrencyRepository from './currencyRepository';

const repositories = {
  country: CountryRepository,
  payment: PaymentRepository,
  currency: CurrencyRepository,
};

export default {
  get: name => repositories[name],
};
