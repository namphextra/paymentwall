export default {
  config: new Map(),

  init() {
    this.config.set(
      'paymentWallProjectKey',
      process.env.PAYMENT_WALL_PROJECT_KEY,
    );
    this.config.set(
      'paymentWallSignVersion',
      process.env.PAYMENT_WALL_SIGN_VERSION,
    );
    this.config.set('currencyAPIKey', process.env.CURRENCY_CONVERT_API_KEY);
  },
};
