export default {
  formatCurrency(money, countryCode, currency) {
    return new Intl.NumberFormat(countryCode, {
      style: 'currency',
      currency,
    }).format(money);
  },
};
